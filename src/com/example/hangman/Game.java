package com.example.hangman;

import java.util.ArrayList;
import java.util.List;

/**
 * Game is a class used by SecondActivity that handles the current word.
 * and the players actions.
 * @author Stjernstr�m
 */

public class Game {
	
	Wordlist words = new SimpleWordlist();
	char chance;
	String word = words.getRandomWord();
	int tries = 0;
	StringBuilder progress = initializeWord(word);
	private static final int  MAX_TRIES = 10;
	private boolean won = false;
	private  List<Character> used_letters;
	
	/**
	 * Takes a String and returns a String of underscores of equal length.
	 * @param w :The word in the game
	 * @return the String of underscores that has the same length of the word.
	 */
	
	public Game() {
		used_letters = new ArrayList<Character>();
	}
	
	/** Checks if a letter has been used before
	 * 
	 * @param c :a letter to check
	 * @return true if the letter has been used before, otherwise false
	 */
	
	public boolean usedLetter(char c){
		return used_letters.contains(Character.toLowerCase(c));
	}
	
	public List<Character> getUsedLetters() {
		return used_letters;
	}
	
	public int getTries() {
		return tries;
	}
	
	/**
	 * Initializes the progress-word (i.e the word to show on screen while guessing).
	 * @param a word to guess
	 * @return the progess word
	 */
	
	public StringBuilder initializeWord(String w) {
		
		String temp = "";
		
		for (int i = 0; i < w.length(); i++) {
			temp += "*";
		}
		progress = new StringBuilder(temp);
		return progress;
	}
	
	/**
	 * Checks if the player has revealed the entire word.
	 * @return True if the player has won, otherwise false.
	 */
	
	public boolean hasWon() {
;		return progress.toString().equals(word);
	}
	
	/**
	 * Checks if the value of the character matches a valid character
	 * @param c :Character that gets checked.
	 * @return true if valid, otherwise false.
	 */
	
	public boolean checkValidity(char c) {
		return (Character.isLetter(c));
	}
	
	/** 
	 * guessLetter lets the player guess a letter in the word.
	 * @param c : The player's guessed letter.
	 * @return true if the word contains char c, otherwise false.
	 **/
	
	public boolean guessLetter(char c) {
		
		boolean match = false;
		
		c = Character.toLowerCase(c);
		used_letters.add(c);
		for (int i = 0; i < word.length(); i++ ) {
			
			if (c == word.charAt(i)) {
				
				
				
				progress.setCharAt(i, c);
				match= true;
			}
		}
		
		if (progress.equals(word)) {
			won = true;
			return won;
		}
		
		if (!match) {
			tries++;
			
		}
		return match;
	}
	
	/**
	 * Getter for the String "progress", which is the String that 
	 * contains the guessed letters in the game.
	 * @return the String "progress".
	 */
	
	public String getCurrentWord() {
		String current = progress.toString();
		return current;
	}
	
	/**
	 * Lets the player attempt to guess the complete word.
	 * @param suggestion :The word that the player guesses.
	 * @return true if correct word, otherwise false.
	 */
	
	public boolean guessWord(String suggestion) {
		
		suggestion = suggestion.trim();	
//		suggestion = suggestion.toLowerCase();
		return (suggestion.equals(word));
	}
	
	/**
	 * Checks if the player has lost the game.
	 * @return true if the players attempts exceed the maximum amount, otherwise false.
	 */
	
	public boolean hasLost() {
		return tries >= MAX_TRIES;
	}
}