package com.example.hangman;

public interface PopUpListener {

	public void onDialogClosed(String word); 

}
