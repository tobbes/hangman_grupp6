package com.example.hangman;

import java.util.LinkedList;
import java.util.List;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Highscore extends ActionBarActivity {

	HighscoreDB scores;
	List<TextView> views = new LinkedList<TextView>();
	
	/**
	 * onCreate initializes the layout of the page with buttons and entries in the 
	 * highScore-list.
	*  @author Tobias Ednersson
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_highscore);

		Button menuButton = (Button) findViewById(R.id.menuButton);
		menuButton.setOnClickListener(buttonClickListener);
		Button resetButton = (Button) findViewById(R.id.resetButton);
		resetButton.setOnClickListener(buttonClickListener);
		scores = new HighscoreDB(this.getApplicationContext());
		
		//for testing purposes only
		/*scores.deleteAll();
		scores.insertNewScore("Nils", 1);
		scores.insertNewScore("Nils", 2);
		scores.insertNewScore("Nils", 3);
		scores.insertNewScore("Nils", 4);
		scores.insertNewScore("Nils", 5);
		scores.insertNewScore("Nils", 6);*/
		List<String> currentScores = scores.getScores();
		
		boolean outcome = scores.isHighScore(5);
		
		Log.d("hopp","Resultatet: "+ outcome+"");
		
		if (currentScores.size() > 0) {
			//Get references to the highscoreviews
			TextView t = (TextView) findViewById(R.id.highScore1);		
			views.add(t);		
			t = (TextView) findViewById(R.id.highScore2);		
			views.add(t);	
			t = (TextView) findViewById(R.id.highScore3);		
			views.add(t);
			t = (TextView) findViewById(R.id.highScore4);
			views.add(t);
			t = (TextView) findViewById(R.id.highScore5);
			views.add(t);				
			populateScores(currentScores);
		}
	}
	
	/**
	 * populateScores reads the entries of HighscoreDB and puts them into the layout of
	 * activity_Highscore.
	 * @param scores: list of Highscore-results.
	 */
	
	private void populateScores(List<String> scores){
		int index = 0;
		for(String result : scores){

			TextView t = views.get(index);
			t.setText(result);
			index++;	
		}
		return;
	}
	
	/**
	 * OnClickListener gives functions to the buttons in activity_Highscore.
	 */
	
	private OnClickListener buttonClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch(v.getId()) {
				case R.id.menuButton:
					startMainActivity();
					break;
				case R.id.resetButton:
					resetScore();
					break;
			}
		}
	};

	/**
	 * onCreateOptionsMenu inflates the menu and adds items to the action bar 
	 * if those are present.
	 */
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.highscore, menu);
		return true;
	}

	/**
	 * onOptionsItemSeleced handles action-bar item-clicks. 
	 * The action bar will automatically handle clicks on the Home/Up button, 
	 * as long as you specify a parent activity in AndroidManifest.xml.
	 */
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Starts the Activity MainActivity.
	 */
	
	private void startMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
	/**
	 * resetScore calls the deleteAll method in HighscoreDB and refreshes the 
	 * Highscore-Activity.
	 */
	
	private void resetScore() {
		scores.deleteAll();
		finish();
		startActivity(getIntent());
	}
}
