package com.example.hangman;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
/**
 * 
 * @author Viktor J
 *	Common class for an PopUpDialog that uses its own interface as a Listener.
 */
public class PopUpDialog extends DialogFragment {
	
	public String input;
	private String title;
	private String text1;
	private String text2;
	private String posBut;
    
    // Use this instance of the interface to deliver action events
    PopUpListener mListener;
    /**
     * Constructor for the class PopUpDialog
     * @param title
     * Sets the title for the dialog
     * @param text1
     * Sets text for TextView1
     * @param text2
     * Sets text for TextView2
     * @param posBut
     * Sets text for the AcceptButton(PossitiveButton)
     */
    public PopUpDialog(String title, String text1, String text2, String posBut) {
    	this.title = title;
    	this.text1 = text1;
    	this.text2 = text2;
    	this.posBut = posBut;
    }
    
    // Override the Fragment.onAttach() method to instantiate the PopUpListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the PopUpListener so we can send events to the host
            mListener = (PopUpListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement PopUpListener");
        }
    }
	/**
	 * Creates and shows the dialog with all the Texts and features.
	 */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.popup_layout, null);
		// Use the Builder class for convenient dialog construction
       final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       
       TextView tv = (TextView)view.findViewById(R.id.infoWholeWordRight_text);
       tv.setText(text1);
       TextView tev = (TextView)view.findViewById(R.id.infoWholeWordWrong_text);
       tev.setText(text2);
       EditText et = (EditText)view.findViewById(R.id.whole_word_guess_edit);
       et.setHint(title);
       
       builder.setView(view);
       
       builder.setMessage(title)
        		.setPositiveButton(title, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id ) {
                	   final EditText wholeWord = (EditText) view.findViewById(R.id.whole_word_guess_edit);
                	   mListener.onDialogClosed(wholeWord.getText().toString());
                	   
                	   dialog.cancel();
                	       	   
                    }
               })
               .setNegativeButton(R.string.popUp_cancel, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   // User cancelled the dialog
                   }
               
               });
        // Create the AlertDialog object and return it
       return builder.create();
    }
}

