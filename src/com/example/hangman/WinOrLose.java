package com.example.hangman;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**This is activity launched when a game ends
 * 
 * @author Philip Stjernström, Tobias Ednersson, Victor Jegeras
 *
 */
public class WinOrLose extends ActionBarActivity implements PopUpListener {
	
	private boolean WinLose;
	private HighscoreDB scores;
	private String playerName;
	MediaPlayer mp;
	private int score;
	OnCompletionListener mplistener = new OnCompletionListener() {
		
		@Override
		public void onCompletion(MediaPlayer mp) {
			mp.reset();
			mp.release();
		}
	};
	
	/**
	 * Initializes the layout and calls several methods to display the correct information 
	 * on the screen and play a sound.
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_win_or_lose);
		Button gameButton = (Button) findViewById(R.id.gameButton);
		gameButton.setOnClickListener(buttonClickListener);
		Button highScoreButton = (Button) findViewById(R.id.highScoreButton);
		highScoreButton.setOnClickListener(buttonClickListener);
		Button menuButton = (Button) findViewById(R.id.menuButton);
		menuButton.setOnClickListener(buttonClickListener);
		mp = MediaPlayer.create(getApplicationContext(),R.raw.applause);

		mp.setOnCompletionListener(mplistener);
		scores = new HighscoreDB(this.getApplicationContext());
		WinLose = getIntent().getExtras().getBoolean("winOrLose");
		gameEnded(WinLose);
		setButtonVisibility(WinLose);
		setTextView(WinLose);
		updateScoreText(WinLose);
		
		
	}
	
	/**
	 * buttonClickListener gives functions to the buttons.
	 */
	
	private OnClickListener buttonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch(v.getId()) {
				case R.id.gameButton:
					startGameActivity();
					break;
				case R.id.highScoreButton:
					startHighScoreActivity();
					break;
				case R.id.menuButton:
					startMainActivity();
					break;
			}
		}
	};
	
	/**
	 * gameEnded is used to display a specific animation depending on whether the player
	 * wins or loses.
	 * @param win: True if player has won, otherwise false.
	 */
	
	@SuppressLint("NewApi") public void gameEnded(boolean win) {
		
		mp.setVolume(1.0f, 1.0f);
		mp.setLooping(false);
		ImageView loseimage = (ImageView) findViewById(R.id.win_lose_image);
		
		if(!win) {
			loseimage.setImageResource(R.drawable.looseanimation);
			mp = MediaPlayer.create(getApplicationContext(),R.raw.laugh);
			
			if(scores.isHighScore(GameState.WinOrLose.getScore()) && GameState.WinOrLose.getScore() > 0) {
				PopUpDialog pud = new PopUpDialog(getResources().getString(R.string.popUp_highScore_Name), 
						getResources().getString(R.string.popUp_highScore_textview1),
						getResources().getString(R.string.popUp_highScore_textview2),
						getResources().getString(R.string.popUp_highScore_Name));
				pud.show(getSupportFragmentManager(), "dialog");
				
			}
		}
		else if(win) {
			loseimage.setImageResource(R.drawable.winanimation);
			
		}
		
		final AnimationDrawable myAnimationDrawable = (AnimationDrawable)loseimage.getDrawable();
		loseimage.post(
				new Runnable() {
				@Override
				public void run() {					
					myAnimationDrawable.start();										
				}
			});
		mp.start();
	}
	
	/**
	 * onDestroy releases mediaplayer when the activity is destroyed.
	 */
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
		mp.release();
	}
	
	/**
	 * onCreateOptions inflates the menu; this adds items to the action bar if it is present.
	 */
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.win_or_lose, menu);
		return true;
	}
	
	/**
	 * onOptionsItemSelected handles action bar item clicks. The action bar will
	 * automatically handle clicks on the Home/Up button, as long
	 * as you specify a parent activity in AndroidManifest.xml.
	 */
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Starts SecondActivity.
	 */
	
	private void startGameActivity() {
		Intent intent = new Intent(this,SecondActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Starts HighScore.
	 */
	
	private void startHighScoreActivity() {
		Intent intent = new Intent(this,Highscore.class);
		startActivity(intent);
	}
	
	/**
	 * Starts MainActivity.
	 */
	
	private void startMainActivity() {
		Intent intent = new Intent(this,MainActivity.class);
		startActivity(intent);
	}
	
	/**
	 * setTextView is used to alter the displayed text in WinOrLoses' layout
	 * @param win: True if the player has won, false if the player lost.
	 */
	
	private void setTextView(boolean win) {
		TextView text = new TextView(this);
	    text = (TextView)findViewById(R.id.winLose);
	    
	    if (win) {
	    	text.setText("You win!");
	    }
	    
	    else if (!win) {
	    	text.setText("You lose");
	    }
	}
	
	/**
	 * GameState is used to store an int that won't reset when you go to other activities
	 * or create a new instance of WinOrLose.
	 * @author Stjernstr�m
	 */
	
	public enum GameState{
		WinOrLose;
		
		private int savedScore;
		private GameState() {
			savedScore = 0; 
		}
		
		public int getScore(){
		    return savedScore;
		}
		
		public void addScore(int score) {
			this.savedScore += score;
		}
		
		public void resetScore() {
			this.savedScore = 0;
		}
	}
	
	/**
	 * setScoreText updates the text that displays the score in WinLose.
	 * @param win : True if a round has been won, otherwise false.
	 */
	
	private void updateScoreText(boolean win) {
		TextView points = new TextView(this);
		points = (TextView)findViewById(R.id.points);
		
		if (!win) {
			points.setText("Final score: " + GameState.WinOrLose.getScore());
			
		}
		
		else if (win) {
			GameState.WinOrLose.addScore(1);
			points.setText("Score: " + GameState.WinOrLose.getScore());
			
		}
	}
	
	private void setButtonVisibility(boolean win) {
		Button gameButton = (Button) findViewById(R.id.gameButton);
		Button highScoreButton = (Button) findViewById(R.id.highScoreButton);
		Button menuButton = (Button) findViewById(R.id.menuButton);
		
		if (!win) {
			gameButton.setText("Retry");
			highScoreButton.setVisibility(View.VISIBLE);
			menuButton.setVisibility(View.VISIBLE);
		}
		
		else if (win) {
			gameButton.setText("Play again");
			highScoreButton.setVisibility(View.GONE);
			menuButton.setVisibility(View.GONE);
		}
	}

	@Override
	public void onDialogClosed(String word) {
		score = GameState.WinOrLose.getScore();
		playerName = word;
		scores.insertNewScore(playerName, score);
		GameState.WinOrLose.resetScore();

	}
}


