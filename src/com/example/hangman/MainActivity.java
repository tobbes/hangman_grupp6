package com.example.hangman;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * This activity presents the applications main menu
 * @author Victor Jegeras, Tobias Ednersson, Philip Stjernström
 *
 */
public class MainActivity extends ActionBarActivity {
	
	/**
	 * Initializes layout.
	 */
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button gameButton = (Button) findViewById(R.id.new_game_button);
        gameButton.setOnClickListener(buttonClickListener);
        Button highscoreButton = (Button) findViewById(R.id.highscore_button);
        highscoreButton.setOnClickListener(buttonClickListener);
	}

	/**
	 * Gives functions to the buttons.
	 */
	
	private OnClickListener buttonClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch(v.getId()) {
				case R.id.new_game_button:
					startGameActivity();
					break;
				case R.id.highscore_button:
					startHighscoreActivity();
					break;
			}
		}
	};
		
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
	
	/**
	 * onOptionsItemSelected handles action bar item clicks. The action bar will
	 * automatically handle clicks on the Home/Up button, so long
	 * as you specify a parent activity in AndroidManifest.xml.
	 */
	
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /**
     * Starts SecondActivity.
     */
    
    public void startGameActivity() {
    	Intent intent = new Intent(this,SecondActivity.class);
		startActivity(intent);
	}
    
    /**
     * Starts HighScore.
     */
    
    public void startHighscoreActivity() {
		Intent intent = new Intent(this,Highscore.class);
		startActivity(intent);
	}
}
