
package com.example.hangman;

import java.util.List;
import com.example.hangman.WinOrLose.GameState;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/** 
 * Main game-activity
 * @author Tobias Ednersson, Philip Stjernström, Victor Jegeras
 */
public class SecondActivity extends ActionBarActivity implements PopUpListener {

	/**
	 * Listener for the Send button in Second Activity.
	 */

	Toast oldLetterWarning;
	private Game game;
	private EditText guess;
	private TextView progress;
	private Button guessbutton;
	private Button wholeWordButton;
	private TextView used_letters_view;
	private boolean endResult;

	
	TextWatcher checkInput = new TextWatcher(){

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		/**
		 * Enables the guessbutton in gameview if a letter is typed
		 * disables it if something else happens
		 */
		@Override
		public void afterTextChanged(Editable s) {

			//the user empties the field
			if (s.length() == 0)
			{
				guessbutton.setEnabled(false);
				return;
			}

			char input =  s.charAt(0);
			if (Character.isLetter(input)){
				if(!guessbutton.isEnabled()) {
					guessbutton.setEnabled(true);
				}

				else {
					if(guessbutton.isEnabled()){
						guessbutton.setEnabled(false);
					}
				}
			}
		}
	};

	private int id, imageNumber;

	private OnClickListener wholeWordClickListener = new OnClickListener()   {

		@Override
		public void onClick(View v) {
			createPopUp();
		}
	};	
	
	/** 
	 * guessButtonClickListener is used by a button to call the method doGuess with the 
	 * argument guessedLetter.
	 */

	private OnClickListener guessButtonClickListener = new OnClickListener()   {

		@Override
		public void onClick(View v) {
			char guessedLetter = guess.getText().charAt(0);
			guess.setText("");
			guessbutton.setEnabled(false);
			doGuess(guessedLetter);
		}
	};

	/**
	 * doGuess is used to guess a letter in the word.
	 * It empties the input-field and checks if the chosen letter has already been used.
	 * If true, it goes on to check if the letter exists in the word, and if it does, it 
	 * updates the respective textViews with the letter, and then checks if the word is 
	 * complete.
	 * 
	 * If the letter doesn't match with the word however, it updates the image to reflect 
	 * how many attempts you have left, as well as the list of used letters.
	 * 
	 * If the player completes the word of runs out of attempts, startWinOrLoseActivity 
	 * is called.
	 * @param guessedLetter: The player's guessed letter.
	 */
	
	private void doGuess(char guessedLetter ) {
		int guesses;
		
		if (game.usedLetter(guessedLetter)) {
			oldLetterWarning.show();
			return;
		}

		if (game.guessLetter(guessedLetter)) {			
			
			String newWord = game.getCurrentWord();
			progress.setText(newWord);
			
			if (game.hasWon()) {
				endResult = true;
				startWinOrLoseActivity();
			}	
		}

		else {
			guesses = game.getTries();
			this.changeImage(guesses);

			if (game.hasLost()) {
				endResult = false;
				startWinOrLoseActivity();
			}
		}
		updateUsedLetters();
	}
	
	/**
	 * Initiates the layout.
	 */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		game = new Game();
		setContentView(R.layout.activity_second);
		guess = (EditText) findViewById(R.id.input_text);
		guessbutton = (Button) findViewById(R.id.send_button);
		wholeWordButton = (Button) findViewById(R.id.wholeWord_button);
		//field for used letters
		used_letters_view = (TextView) findViewById(R.id.used_letters_text);
		//Set up warningpopup if user uses already used letter.
		oldLetterWarning = Toast.makeText(this.getApplicationContext(), "This letter has already been used", Toast.LENGTH_SHORT);
		
		guess.addTextChangedListener(checkInput);
		guessbutton.setOnClickListener(guessButtonClickListener);
		
		wholeWordButton.setOnClickListener(wholeWordClickListener);
		//Add a listener to the guessfield which enables guessbutton when a letter is entered
		//guess.setKeyListener(editFieldListener);
		progress = (TextView) findViewById(R.id.progress_text);
		progress.setText(game.getCurrentWord());
	}

	/**
	 * onCreateOptionsMenu inflates the menu; this adds items to the action bar 
	 * if it is present.
	 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}

	/**
	 * Action-bar item-clicks get handled here. The action-bar will
	 * automatically handle clicks on the Home/Up button as long
	 * as you specify a parent activity in AndroidManifest.xml.
	 */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * changeImage is used to update the imageView.
	 * The selected image is dependent on how many attempts the player has left.
	 */
	
	public void changeImage(int numberOfguesses) {
		ImageView image = (ImageView) findViewById(R.id.hangman_image);
		imageNumber = numberOfguesses;
		id = getResources().getIdentifier("hm"+imageNumber, "drawable", getPackageName());
		image.setImageResource(id);
		
	}
	
	/**
	 * startWinOrLoseActivity creates an Intent of the class WinOrLose 
	 * with the boolean "endResult" added in, which is used in said class to 
	 * display the appropriate result.
	 */
	
	public void startWinOrLoseActivity() {
		Intent intent = new Intent(this,WinOrLose.class);
		intent.putExtra("winOrLose", endResult);
		startActivity(intent);
		
	}	
	
	public void createPopUp() {
		PopUpDialog ad = new PopUpDialog(getResources().getString(R.string.popUp_wholeWordGuess), 
				getResources().getString(R.string.popUp_wholeWordRight),
				getResources().getString(R.string.popUp_wholeWordWrong),
				getResources().getString(R.string.popUp_wholeWordGuess));
		ad.show(getSupportFragmentManager(), "dialog");
	}
	
	private void updateUsedLetters() { 
		List<Character> used = game.getUsedLetters();
		StringBuilder builder = new StringBuilder();
		builder.append(used);						 
		used_letters_view.setText(builder);
	}

	@Override
	public void onDialogClosed(String word) {
		if(game.guessWord(word)) {
			endResult = true;
			GameState.WinOrLose.addScore(2);
			startWinOrLoseActivity();
		}else {
			endResult = false;
			startWinOrLoseActivity();
		}
	}
}
