package com.example.hangman;

import java.util.LinkedList;
import java.util.List;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

/**
 * A databasebackend for the highscore activity
 * @author Tobias Ednersson
 *
 */


public class HighscoreDB extends SQLiteOpenHelper {
	private static int version = 2;
	private static String name = "HighScores";
	private String createStatement =
			"CREATE TABLE scores(name VARCHAR(30), score INT)";
	private SQLiteDatabase db = null;
	private static final int PLACES_ON_LIST = 5;


	/**
	 * Empties the database
	 */
	public void deleteAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete("scores", null, null);
	}

	/**
	 * 
	 * @param context - the applicationcontext of the activity which instantiates
	 *  the databasehelper
	 */
	public HighscoreDB(Context context) {
		super(context, name, null, version);
		db = this.getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(createStatement);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}

	/**
	 * removeLowest removes the entry with the lowest score from the list
	 * if there are more than PLACES_ON_LIST entries
	 */

	public void removeLowest() {
		SQLiteDatabase db = this.getWritableDatabase();
		String countSQL = "SELECT count(*) from scores";
		Cursor c = db.rawQuery(countSQL, null);	
		c.moveToFirst();

		int num = c.getInt(0);

		if(num >= PLACES_ON_LIST){

			db.delete("scores", "score = (select min(score) from scores)", null);
		}
	}

	/**
	 * Inserts a new score into the highscore database
	 * it also automatically removes the lowest score from the database;
	 * @param player -- the name of the highscorer
	 * @param score -- his/her score
	 */

	public void insertNewScore(String player,int score){
		SQLiteDatabase wdb = this.getWritableDatabase();

		// If the highscorelist is full, this removes the lowest score.
		this.removeLowest();

		SQLiteStatement stmt = wdb.compileStatement("INSERT INTO scores VALUES(?,?)");	

		stmt.bindString(1,player);
		stmt.bindLong(2,score);
		stmt.execute();		
	}

	/**
	 * Gets the scores from the database
	 * @return -- the scores in the database
	 */

	public List<String> getScores(){
		List<String> toReturn = new LinkedList<String>();
		Cursor c = db.rawQuery("SELECT * FROM scores ORDER BY score DESC", null);

		while(c.moveToNext()) {
			String name = c.getString(0);
			int res = c.getInt(1);
			toReturn.add(name + " " + res);
		}

		return toReturn;
	}

	/**
	 * Checks if a score is a new highscore.
	 * @param playersScore: A new score to compare.
	 * @return True if the score is higher than at at least one of the scores in the
	 * highscorelist, otherwise false.
	 */

	public boolean isHighScore(int playersScore){
		SQLiteDatabase db = this.getReadableDatabase();
		String[] args = {playersScore+""};
		//Cursor c = db.rawQuery("(SELECT * FROM scores WHERE score < ?) OR ", args );

		String statement =  "SELECT (EXISTS(SELECT * FROM scores WHERE score < ? ) OR (SELECT COUNT(*) FROM scores) < "+ PLACES_ON_LIST + ")"; ;
		Cursor c = db.rawQuery(statement, args);

		c.moveToFirst();

		return c.getInt(0) > 0;

	}

}


