package com.example.hangman;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * A simple implementation of the Wordlist interface, a manually constructed list of words.
 * @author Tobias Ednersson
 */

public class SimpleWordlist implements Wordlist {

	private List<String> words;
	Random rand;
	
	/**
	 * SimpleWordList contains the words that are used in the game in an ArrayList.
	 * @throws IOException 
	 */
	
	public SimpleWordlist() {
		words = new ArrayList<String>();

		words.add("book");
		words.add("chair");
		words.add("axe");
		words.add("hangman");
		words.add("vibration");
		words.add("teacher");
		words.add("stranger");
		words.add("cat");
		words.add("owl");
		words.add("final");
		words.add("production");
		words.add("programming");
		words.add("android");
		words.add("raw");
		words.add("ending");
		words.add("banter");
		words.add("wall");
		words.add("cereal");
		words.add("orientation");
		words.add("jaguar");
		words.add("extreme");
		words.add("verbose");
		words.add("queen");
		words.add("bee");
		words.add("underwhelming");
		words.add("overwhelming");
		words.add("inside");
		words.add("year");
		words.add("shelf");
		words.add("table");
		words.add("xylophone");
		words.add("trumpet");
		words.add("especially");
		words.add("arduous");
		words.add("arrogant");
		words.add("gravel");
		words.add("dynamite");
		words.add("goo");
		words.add("duck");
		words.add("stupid");
		words.add("angry");
		words.add("happy");
		words.add("sad");
		words.add("glad");
		words.add("disappointed");
		words.add("funny");
		words.add("weak");
		words.add("bicycle");
		words.add("asinine");
		words.add("meandering");
		words.add("alas");
		words.add("poignant");
		words.add("terrible");
		words.add("excruciating");
		words.add("tea");
		words.add("coffee");
		words.add("cake");
		words.add("cavern");
		words.add("canoe");
		words.add("ridiculous");
		words.add("silly");
		words.add("brilliant");
		words.add("car");
		words.add("boat");
		words.add("unused");
		words.add("chaos");
		words.add("serene");
		words.add("power");
		words.add("loyal");
		words.add("episode");
		words.add("hate");
		words.add("love");
		words.add("brown");
		words.add("green");
		words.add("black");
		words.add("red");
		words.add("blue");
		words.add("yellow");
		words.add("pink");
		words.add("purple");
		words.add("teal");
		words.add("orange");
		words.add("white");
		words.add("gray");
		words.add("insane");
		words.add("innocuous");
		words.add("tragic");
		words.add("hestitation");
		words.add("insidious");
		words.add("malicious");
		words.add("help");
		words.add("great");
		words.add("leave");
		words.add("you");
		words.add("have");
		words.add("been");
		words.add("helpful");
		words.add("and");
		words.add("eye");
		words.add("do");
		words.add("not");
		words.add("know");
		words.add("what");
		words.add("to");
		words.add("write");
		words.add("next");
		words.add("city");
		words.add("village");
		words.add("ground");
		words.add("joke");
		words.add("poison");
		words.add("cure");
		words.add("medicine");
		words.add("hospital");
		words.add("ready");
		words.add("definitely");
		words.add("disease");
		words.add("demographic");
		words.add("demon");
		words.add("dark");
		words.add("doubt");
		words.add("weather");
		words.add("whether");
		words.add("nice");
		words.add("ore");
		words.add("ornament");
		words.add("okay");
		words.add("oscilloscope");
		words.add("ring");
		words.add("reach");
		words.add("farm");
		words.add("freak");
		words.add("insignificant");
		words.add("enter");
		words.add("key");
		words.add("shift");
		words.add("bravo");
		words.add("arm");
		words.add("leg");
		words.add("chest");
		words.add("head");
		words.add("some");
		words.add("see");
		words.add("scathing");
		words.add("negative");
		words.add("positive");
		words.add("apple");
		words.add("banana");
		words.add("grape");
		words.add("pear");
		words.add("here");
		words.add("mere");
		words.add("sneer");
		words.add("shear");
		words.add("bear");
		words.add("monkey");
		words.add("lion");
		words.add("on");
		words.add("range");
		words.add("cow");
		words.add("lime");
		words.add("lemon");
		words.add("crave");
		words.add("demand");
		words.add("dream");
		words.add("if");
		words.add("slap");
		words.add("hit");
		words.add("punch");
		words.add("slash");
		words.add("crush");
		words.add("vapid");
		words.add("ignorant");
		words.add("hurt");
		words.add("flame");
		words.add("fire");
		words.add("water");
		words.add("flood");
		words.add("sea");
		words.add("ocean");
		words.add("fork");
		words.add("knife");
		words.add("dinner");
		words.add("jungle");
		words.add("job");
		words.add("me");
		words.add("but");
		words.add("wind");
		words.add("horse");
		words.add("trap");
		words.add("star");
		words.add("stream");
		words.add("lure");
		words.add("gum");
		words.add("chicken");
		words.add("meat");
		words.add("sheet");
		words.add("cheat");
		words.add("savor");
		words.add("get");
		words.add("give");
		words.add("neat");
		words.add("neat");
		words.add("beat");
		words.add("elite");
		words.add("meet");
		words.add("heat");
		words.add("greet");
		words.add("cow");
		words.add("bow");
		words.add("now");
		words.add("hunt");
		words.add("referee");
		words.add("reference");
		words.add("refer");
		words.add("cry");
		words.add("fry");
		words.add("awry");
		words.add("tilt");
		words.add("tree");
		words.add("lose");
		words.add("win");
		words.add("choose");
		words.add("hose");
		words.add("noose");
		words.add("excecution");
		words.add("executive");
		words.add("leader");
		words.add("front");
		words.add("back");
		words.add("left");
		words.add("right");
		words.add("up");
		words.add("down");
		words.add("sideways");
		words.add("forward");
		words.add("backward");
		words.add("diagonal");
		words.add("triangle");
		words.add("test");
		words.add("top");
		words.add("tool");
		words.add("zebra");
		words.add("verify");
		words.add("varying");
		words.add("floor");
		words.add("foot");
		words.add("hand");
		words.add("finger");
		words.add("ear");
		words.add("triangle");
		words.add("circle");
		words.add("square");
		words.add("rectangle");
		words.add("oval");
		words.add("den");
		rand = new Random();
		
	}
	
	/**
	 * getRandomWord creates a random number and uses that to pick and return a word.
	 */
	
	@Override
	public String getRandomWord() {
		int randomIndex = rand.nextInt(countWords());
		return words.get(randomIndex);
	}

	/**
	 * Getter for the amount of words in the ArrayList.
	 */
	
	@Override
	public int countWords() {
		return words.size();
	}
	
}
