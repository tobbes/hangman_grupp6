/** 
 * @author Tobias Ednersson

* This interface should be implemented by any wordlist to be used with the hangman app
* 
**/

package com.example.hangman;

public interface Wordlist {

	/** 
	 * @return a random word from this wordlist
	 */
	public String getRandomWord();
	
	
	/**
	 * 
	 * @return the number of words in this wordlist
	 */
	public int countWords();

}